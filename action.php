<?php
include('common.php');
header('Content-Type: application/json; charset=UTF-8');

$db      = DB::getInstance() or die('No APP ID specified');
$actions = $db->get('actions', array());
$data    = array(
  'status'  => 'success',
  'actions' => $actions,
);

echo json_encode($data);
$db->set('actions', array());
