<?php
include('common.php');
header('Content-Type: application/json; charset=UTF-8');

if ( empty( $_POST['info'] ) )
  die(json_encode(array('status' => 'error')));

$data     = json_decode($_POST['info'], true);
$db       = DB::getInstance() or die('No APP ID specified');
$response = array(
  'status'  => 'success',
);

$db->set('info', $data);

echo json_encode($data);
