<?php
include('common.php');
function htmlesc($string) {
  return htmlspecialchars($string, ENT_QUOTES, 'UTF-8');
}
function urlesc($string) {
  return rawurlencode($string);
}
function jsonesc($data) {
  return json_encode($data, JSON_HEX_QUOT | JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS);
}

$db          = DB::getInstance() or die('No APP ID specified');
$tpl         = new Template('index.phtml');
$tpl->db     = $db;
$tpl->appkey = $db->prefixkey;

$actions = $db->get('actions', array());
$target  = @$_REQUEST['action'];
$tabid   = intval(@$_REQUEST['tabid']);
$url     = @$_REQUEST['url'];

switch($target) {
  case 'navigate':
    
    if (!$tabid or !$url)
      die('wrong parameters');
    
    $actions[] = array(
      'action' => 'navigatetab',
      'tabid'  => $tabid,
      'url'    => $url,
    );
    
    $db->set('actions', $actions);
    die('ok');
    break;
  
  case 'refresh':
    
    if (!$tabid)
      die('wrong parameters');
    
    $actions[] = array(
      'action' => 'refreshtab',
      'tabid'  => $tabid,
    );
    
    $db->set('actions', $actions);
    die('ok');
    break;
  
  case 'close':
    
    if (!$tabid)
      die('wrong parameters');
    
    $actions[] = array(
      'action' => 'closetab',
      'tabid'  => $tabid,
    );
    
    $db->set('actions', $actions);
    die('ok');
    break;
  
  case 'getinfo':
    header('Content-Type: application/json; charset=UTF-8');
    die(json_encode($db->get('info', array())));
    break;
  
  default:
    break;
}

$tpl->info = $db->get('info', array());
$tpl->render();
