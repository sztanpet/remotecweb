var webapp = function(info) {
  this.polltimer;
  this.pollseconds = 5;
  this.pollurl     = 'index?action=getinfo';
  this.info        = info || '[]';
  
  $('.refresh a, .close a').live('click', this.onClick);
  $('.modify').live('click', this.onModify);
  $('.cancel').live('click', this.onCancel);
  $('.navigate form').live('submit', this.onSubmit);
  this.poll();
  
};
webapp.prototype.poll = function() {
  
  if (this.polltimer)
    clearTimeout(this.polltimer);
  
  var self = this;
  $.get(this.pollurl, function(data, status, xhr) {
    
    self.polltimer = setTimeout($.proxy(self.poll, self), self.pollseconds * 1000);
    
    if (!data)
      return;
    
    if (self.info == xhr.responseText)
      return;
    
    self.updateInfo(data);
    self.info = xhr.responseText;
    
  });
  
};
webapp.prototype.updateInfo = function(tabs) {
  var currenttabs   = $('#tabs .tab:not(#emptytab)'),
      self          = this,
      currenttabids = [];
  
  if (currenttabs.length == 0 && tabs.length != 0) {
    
    for (var i = 0, j = tabs.length; i < j; i++ ) {
      var tab = this.createTab(tabs[i]);
      
      $(tab).appendTo('#tabs:not(#emptytab)');
      
    }
    
    return;
    
  }
  
  currenttabs.each(function(index) {
    
    elem = $(this);
    var tabindex = self.tabExists(tabs, elem.attr('data-tabid'));
    if ( !tabindex ) {
      
      elem.remove();
      return;
      
    } else
      currenttabids.push(1 * elem.attr('data-tabid'));
    
    var tab = tabs[tabindex];
    if ( elem.find('.title h2.heading').text() != tab.title )
      elem.find('.title h2.heading').text(tab.title);
    
    if ( elem.find('.title .link').attr('href') != tab.url ) {
      
      elem.find('.navigate .navigateformurl').val(tab.url);
      elem.find('.title .link').attr('href', tab.url);
      elem.find('.title .link').text(tab.url);
      
    }
    
    if ( elem.find('.title .status').text() != tab.status )
      elem.find('.title .status').text(tab.status);
    
  });
  
  for (var i = 0, j = tabs.length; i < j; i++) {
    var tab   = tabs[i],
        index = $.inArray(tab.tabid, currenttabids);
    
    if (index !== -1)
      continue;
    
    var tabelem = this.createTab(tab),
        after   = '#emptytab';
    
    if (i !== 0) {
      
      var prev = tabs[i - 1];
      after    = '#tab' + prev.tabid;
      
    }
    
    $(tabelem).insertAfter(after);
    
  }
  
};
webapp.prototype.tabExists = function(tabs, tabid) {
  
  for(var i = 0, j = tabs.length; i < j; i++) {
    var tab = tabs[i];
    
    if ( tab.tabid == tabid )
      return i;
    
  }
  
  return false;
  
};
webapp.prototype.createTab = function(tab) {
  var newtab = $('#emptytab').clone();
  newtab.attr('id', 'tab' + tab.tabid);
  newtab.attr('data-tabid', tab.tabid);
  
  newtab.find('.navigate .navigateformtabid').val(tab.tabid);
  newtab.find('.navigate .navigateformurl').val(tab.url);
  
  newtab.find('.title h2.heading').text(tab.title);
  newtab.find('.title .link').attr('href', tab.url)
  newtab.find('.title .link').text(tab.url);
  newtab.find('.title .status').text(tab.status);
  
  var refreshlink = newtab.find('.refresh a').attr('href');
  refreshlink = refreshlink.replace(/_TABID_/, tab.tabid);
  newtab.find('.refresh a').attr('href', refreshlink);
  
  var closelink = newtab.find('.close a').attr('href');
  closelink     = closelink.replace(/_TABID_/, tab.tabid);
  newtab.find('.close a').attr('href', closelink);
  
  return newtab;
  
};
webapp.prototype.onClick = function(e) {
  e.preventDefault();
  
  $.get($(this).attr('href'));
  
};
webapp.prototype.onSubmit = function(e) {
  e.preventDefault();
  
  $(this).parents('.tab').find('.cancel').click();
  $.post(
    $(this).attr('action'),
    $(this).serialize()
  );
  
};
webapp.prototype.onModify = function(e) {
  e.preventDefault();
  
  var tab = $(this).parents('.tab');
  tab.find('.title, .modify').hide();
  tab.find('.navigateformurl, .cancel, .submit').show();
  tab.find('.navigate').addClass('active');
};
webapp.prototype.onCancel = function(e) {
  e.preventDefault();
  
  var tab = $(this).parents('.tab');
  tab.find('.navigate').removeClass('active');
  tab.find('.navigateformurl, .cancel, .submit').hide();
  tab.find('.title, .modify').show();
};