<?php
error_reporting(E_ALL);

class DB {
  public $db;
  public $prefixkey;
  
  public static function getInstance($host = 'localhost', $port = '11211') {
    
    $appkey = isset($_REQUEST['key'])? $_REQUEST['key']: '';
    if ( !preg_match('/^[a-zA-Z0-9]{1,50}$/', $appkey ) )
      return false;
    
    return new DB($host, $port, $appkey);
    
  }
  
  public function __construct($host, $port, $prefixkey) {
    $this->prefixkey = $prefixkey;
    $this->db        = new Memcached();
    $this->db->addServer($host, $port);
  }
  
  public function generateKey($key) {
    return $this->prefixkey . '-' . $key;
  }
  
  public function get($key, $defaultvalue = null) {
    
    $key = $this->generateKey($key);
    if (!$key)
      return false;
    
    return $this->db->get($key, function($db, $key, &$value) use ($defaultvalue) {
      $value = $defaultvalue;
      return true;
    });
    
  }
  
  public function set($key, $value) {
    
    $key = $this->generateKey($key);
    if (!$key)
      return false;
    
    return $this->db->set($key, $value, 60*60*24*2);
    
  }
  
}

class Template {
  public $template;
  public $variables = array();
  
  public function __construct( $template ) {
    $this->template = $template;
  }
  
  public function __set( $name, $value ) {
    $this->variables[ $name ] = $value;
  }
  
  public function __get( $name ) {
    return $this->variables[ $name ];
  }
  
  public function __isset($name) {
    return isset($this->variables[$name]);
  }
  
  public function __unset($name) {
    unset($this->variables[$name]);
  }
  
  public function clearVars() {
    $this->variables = array();
  }
  
  public function getVars() {
    return $this->variables;
  }
  
  public function render($template = null) {
    
    extract( $this->variables );
    include($template ?: $this->template);
    
  }
  
}
